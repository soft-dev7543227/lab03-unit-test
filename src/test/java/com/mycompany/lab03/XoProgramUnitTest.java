/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class XoProgramUnitTest {

    public XoProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBy_X() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinRow1By_O_output_true() {
        char[][] board = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinRow2By_O_output_true() {
        char[][] board = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinRow3By_O_output_true() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol1By_O_output_true() {
        char[][] board = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }

    @Test
    public void testCheckWinCol2By_O_output_true() {
        char[][] board = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testCheckWinCol3By_O_output_true() {
        char[][] board = {{'-', '-', 'O'}, {'-', 'i', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonalOneBy_O_output_true() {
        char[][] board = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testCheckDiagonalTwoBy_O_output_true() {
        char[][] board = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isWin(board, currentPlayer));
    }
    
    @Test
    public void testChechDraw_output_true() {
        char[][] board = {{'X', 'O', 'X'}, {'O', 'O', 'X'}, {'X', 'X', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.isDraw(board, currentPlayer));
    }
}
