/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
class OXProgram {

    static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row;
    static int col;

    static void printWelcome() {
        System.out.println("Welcome to Ox Game.");
    }

    static void printBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print("| " + board[row][col] + " ");
            }
            System.out.println("|\n-------------");
        }
    }

    static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print(currentPlayer + " turn." + " " + "Please input row[1-3] and column[1-3] : ");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (board[row][col] == '-') {
                board[row][col] = currentPlayer;
                break;
            }
        }
    }

    static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static boolean isWin(char[][] board, char currentPlayer) {
        if (checkRow(board, currentPlayer) || checkCol(board, currentPlayer) || checkDiagonal(board, currentPlayer)) {
            return true;
        }
        return false;
    }

    static boolean checkRow(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            boolean isRowWin = true;
            for (int col = 0; col < 3; col++) {
                if (board[i][col] != currentPlayer) {
                    isRowWin = false;
                    break;
                }
            }
            if (isRowWin) {
                return true;
            }
        }
        return false;
    }

    static boolean checkCol(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            boolean isColWin = true;
            for(int row=0; row<3; row++) {
                if(board[row][i] != currentPlayer) {
                    isColWin = false;
                    break;
                }
            }
            if(isColWin) {
                return true;
            }

        }
        return false;
    }

    static boolean checkDiagonal(char[][] board, char currentPlayer) {
        // check L to R
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }

        // R to L
        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }

        return false;
    }

    static boolean isDraw(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static void printWin() {
        System.out.println(currentPlayer + " Win!!!!");
    }

    static void printDraw() {
        System.out.println("Draw!!");
    }

    static void resetGame() {
        currentPlayer = 'X';
        board = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    }

    static boolean inputContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continue? (y/n): ");
        String input = sc.nextLine();
        return input.equalsIgnoreCase("y");
    }

    public static void main(String[] args) {
        printWelcome();
        boolean continueGame = true;

        while (continueGame) {
            printBoard();
            inputRowCol();

            if (isWin(board, currentPlayer)) {
                printBoard();
                printWin();
                continueGame = inputContinue();
                if (continueGame) {
                    resetGame();
                }
            } else if (isDraw(board, currentPlayer)) {
                printBoard();
                printDraw();
                continueGame = inputContinue();
                if (continueGame) {
                    resetGame();
                }
            } else {
                switchPlayer();
            }
        }
    }

}
